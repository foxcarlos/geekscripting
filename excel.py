def exportar_excel(tupla):
    '''Parametros recibidos:1 Lista o Tupla
    Ej: exportar_excel([('Nombre', 'apellido'), ('Carlos', 'Garcia'), ('Carla', 'Patricia')])
    Metodo que permite tomar una lista o tupla y llevarla a MS Excel mediante la Biblioteca
    xlwt (http://www.python-excel.org/), si ud cree que la lista puede contener mas de 65000 registros es necesario que antes
    de ejecutar el metodo exportar_excel() se ejecute el metodo calcular_longitud ya que este
    me permitira controlar que una hoja no exceda de los 65000 registros permitido, como es
    bien sabido MS Excel solo perite 65000 filas por cada hoja.
    Para mayor detalle de como se realiza ver el metodo, calcular_lontitud()
    '''

    long_reg = len(tupla)
    if long_reg > 0:
        libro = Workbook()
        #Crear las Hojas que se considere necesario
        for hojas in range(long_reg):
            nombre_hoja = 'Hoja %s' % (hojas)
            libro.add_sheet(nombre_hoja)
        #Recorre las tuplas o lista contenidos dentro de la lista mayor pasada como parametro
        for reg_en_hojas in tupla:
            hoja = tupla.index(reg_en_hojas)
            #selecciona la Hoja Actual como predertminada segun en Index de la lista o tupla
            hoja_actual = libro.get_sheet(hoja)
            for registro in reg_en_hojas:
                fila = reg_en_hojas.index(registro)
                #recorre cada campo y lo inserta dentro de la hohja de MS Excel
                for campo in registro:
                    columna = registro.index(campo)
                    print 'Insertar el registro %s en el Fila %s y Columna %s' % (campo, fila, columna)
                    hoja_actual.write(fila, columna, campo)
        #cuando salga del ciclo se guarda el archivo
        nombre_archivo = raw_input('Guardar archivo Excel Como.. :')
        libro.save(nombre_archivo)
