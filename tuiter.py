import tweepy

'''
http://api.twitter.com/oauth/authorize?oauth_token=LyMRjSnataRCAUXWCBlnPNiDDrGtM1Qs5W2D6bw
'''
auth.get_access_token(verifier)

ACCESS_KEY = auth.access_token.key
ACCESS_SECRET = auth.access_token.secret


class tuiter():

    '''Clase de Prueba para el manejo una cuenta en Twitter con la Api Tweepy,
       en estado de testing.
    '''

    def __init__(self, consumer_key, consumer_secret, access_token, access_token_secret):
        self.varlor_devuelto = True
        self.auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        self.auth.set_access_token(access_token, access_token_secret)
        self.api = tweepy.API(self.auth)

    def enviar(self, mensaje):
        '''
        Permite enviar un mensaje al Twitter
        Parametro recibido string Text, que es el mensaje que desea enviar
        '''
        self.api.update_status(mensaje)

    def mencion(self):
        '''
        - Metodo que permite buscar menciones en Twitter y los coloca todos en una lista.
        - Valor devuelto una tupla() con todas las menciones en la cual contiene:
        (fecha,usuario,texto)
        '''
        mencion = self.api.mentions()
        lista_mencion = []
        for lista in mencion:
            if not lista.favorited:
                texto = lista.text
                usuario = lista.author.screen_name
                fecha = lista.created_at.strftime('%d/%m/%Y/ %I:%M:%S')

                texto_sin_mencion = texto.lower().replace('@textogratis', '')
                sin_acentos = unicode(texto_sin_mencion)
                texto_final = normalize('NFKD', sin_acentos).encode('ASCII', 'ignore')
                lista_mencion.append((fecha, usuario, texto_final))
                #lista.favorite()
        return lista_mencion

    def dm_eliminar_todos(self, id_enviado):
        '''
        Busca todos los mensajes directos en la cuenta twitter
        y los elimina por su id
        '''
        dm = self.api.direct_messages()
        for msg in dm:
            id_dm = msg.id
            self.api.destroy_direct_message(id_dm)
            #print 'se limino'

    def dm_eliminar(self, dm_id):
        '''
        Elimina un Mensaje Directo de una Cuenta Twitter por su id.
        Parametro Recibido: (direct message id) del mensaje directo de Twitter
        '''
        self.api.destroy_direct_message(dm_id)

    def dm_recibir(self, cuenta):
        '''
        Busca todos los mensajes directos en la cuenta twitter
        parametro Devuelto una Tupla() con el (Id, direct message)
        '''
        dm = self.api.direct_messages()
        lista_dm = []
        for msg in dm:
            dm_id = msg.id
            dm_enviado_por = msg.sender_screen_name
            sin_acentos = unicode(msg.text)
            texto_final = normalize('NFKD', sin_acentos).encode('ASCII', 'ignore')
            lista_dm.append((dm_id, dm_enviado_por, texto_final))
        return lista_dm

    def seguir_quien_me_sigue(self):
        for seguidor in self.api.followers():
            if not seguidor.following:
                seguidor.follow()
